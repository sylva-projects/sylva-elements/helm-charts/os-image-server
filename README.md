# OS image server

This helm chart aims to deploy on Sylva management cluster a web server hosting some OS images. These OS images are used to bootstrap baremetal workload cluster machines.

## Usage

This helm chart deploys statefulsets which generate pods serving os image files. These files are downloaded from external resources during init phase.

OS images are specified into `os_images` map value:

```bash
$ cat values.yaml
[...]
os_images:
  ubuntu-k8s-1.24.14-rke2:
    uri: "oci://my-registry.org/image-repo/my_image:v1.0.0"
    filename: "ubuntu-k8s-1.24.14-rke2.tar.gz"
    sha256: 48a811ed37ec21235e2e82434519e9aab489cbbbee026df44edcd64e7ba08ccd
  sles-k8s-1.25.5-kubeadm:
    uri: "https://some-web-resource.org/some-path/image.tar.gz"
    filename: "sles-k8s-1.25.5-kubeadm.tar.gz"
```

This example allows 2 images to be downloaded from external URIs (http and oci are supported). Images will be available through ingress at following URLs:

* https://_cluster-url_/ubuntu-k8s-1.24.14-rke2.tar.gz
* https://_cluster-url_/sles-k8s-1.25.5-kubeadm.tar.gz

A optional (but recommended) SHA256 checksum can be provided to verify image integrity.

In any case SHA256 checksum files are also generated and served along with image files:

* https://_cluster-url_/ubuntu-k8s-1.24.14-rke2.tar.gz.sha256sum
* https://_cluster-url_/sles-k8s-1.25.5-kubeadm.tar.gz.sha256sum

For OCI artifacts, it is assumed that the artifact contains a single file; this file will be the one exposed at the final URL ; it does not need to be named as the `.filename` attribute.
If the file contained in the artifact ends in `.gz`, it will be `gunzip`'ed after downloading the artifact and the uncompressed file will be exposed.

In order to be sure there is enough disk space to download and uncompress the file, we calculate the needed size of the PV using information from `os_images.x.{size,archive-size}` (typically fed into this chart by the sylva-units chart via the os-images-info ConfigMap). If the default PV size is greater than this calculated value, then the maximum value will be retained.

At least image URI (`os_images.<image-id>.uri`) and image filename (`os_images.<image-id>.filename`) must be specified in values:

```bash
helm upgrade --install test charts/os-image-server/ --set os_images.my-image.uri=<my-image-uri> --set os_images.my-image.filename=my_image.tar.gz
```

## Scaling up pods

Image are stored on volume managed by PVC (one volume per image shared).
It is possible to scale up statefulset to have more than 1 pod per image but this will only work if images are stored on PVC with ReadWriteMany access mode.

## Parameters (values)

### Common parameters

| Name                 | Description                                        |   Default value    |
|----------------------|----------------------------------------------------|:------------------:|
| `nameOverride`       | String to partially override common.names.fullname |        `""`        |
| `fullnameOverride`   | String to fully override common.names.fullname     |        `""`        |
| `imagePullSecrets`   | Array of secrets used to pull private images       |        `[]`        |
| `podSecurityContext` | Security config for pods                           | `{fsGroup: 10000}` |
| `affinity`           | Affinity for pod assignment                        |        `{}`        |
| `nodeSelector`       | Node labels for pod assignment                     |        `{}`        |
| `tolerations`        | Tolerations for pod assignment                     |        `[]`        |
| `podAnnotations`     | Annotations to add to all deployed pods            |        `{}`        |

### Nginx parameters

| Name                       | Description                         |       Default value        |
|----------------------------|-------------------------------------|:--------------------------:|
| `nginx.resources.limits`   | Nginx container limits              |     `{memory: 128Mi}`      |
| `nginx.resources.requests` | Nginx container requests            | `{memory: 64Mi, cpu: 20m}` |
| `nginx.securityContext`    | Security config for nginx container |      see values.yaml       |

### Downloader parameters

| Name                            | Description                              |       Default value        |
|---------------------------------|------------------------------------------|:--------------------------:|
| `downloader.oci_timeout`        | Timeout for oci artifact download        |           `30m`            |
| `downloader.resources.limits`   | Downloader container limits              |     `{memory: 128Mi}`      |
| `downloader.resources.requests` | Downloader container requests            | `{memory: 64Mi, cpu: 20m}` |
| `downloader.proxy`              | Proxy URL used to download image         |            `""`            |
| `downloader.no_proxy`           | Proxy bypass hosts                       |            `""`            |
| `downloader.extra_ca_certs`     | Additional CA certs to be trusted for pulling artifacts (one string with the content of a PEM format file)|            `""`            |
| `downloader.securityContext`    | Security config for downloader container |      see values.yaml       |

### OS image parameters

| Name                                      | Description                                 |   Default value   |
|-------------------------------------------|---------------------------------------------|:-----------------:|
| `os_images`                               | Map containing description of each image    |       `{}`        |
| `os_image_selectors`                      | Map of criteria to entries in os_images     |       `{}`        |
| `osImagePersistenceDefaults.enabled`      | Default value for image persistence         |      `true`       |
| `osImagePersistenceDefaults.size`         | Default value for image volume size         |       `1Gi`       |
| `osImagePersistenceDefaults.storageClass` | Default value for image volume storageClass |       null        |
| `osImagePersistenceDefaults.accessMode`   | Default value for image volume access mode  | `"ReadWriteOnce"` |

Each os_images entry can contains:
| Name                       | Description                                                  |           Default value           |
|----------------------------|--------------------------------------------------------------|:---------------------------------:|
| `replicaCount`             | set number of pods to use for this image                     |                `1`                |
| `uri`                      | OS image URI (can be http(s) or oci)                         |             mandatory             |
| `filename`                 | OS image filename                                            |             mandatory             |
| `checksum`                 | OS image SHA256 checksum (optional but recommended)          | from `osImagePersistenceDefaults` |
| `persistence.enabled`      | OS image persistence (recommended)                           | from `osImagePersistenceDefaults` |
| `persistence.size`         | OS image volume size (must be bigger than actuel image size) | from `osImagePersistenceDefaults` |
| `persistence.storageClass` | OS image volume storage class                                | from `osImagePersistenceDefaults` |
| `persistence.accessMode`   | OS image volume access mode                                  | from `osImagePersistenceDefaults` |

### Traffic Exposure Parameters

| Name                  | Description                                                                                                                      |  Default value  |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------|:---------------:|
| `service.type`        | Nginx Service type                                                                                                               |   `ClusterIP`   |
| `service.port`        | Nginx service HTTP port                                                                                                          |     `8080`      |
| `service.annotations` | Nginx service extra annotations                                                                                                  |     `8080`      |
| `ingress.enabled`     | Enable ingress record generation for Nginx                                                                                       |     `true`      |
| `ingress.className`   | Ingress class                                                                                                                    |      `""`       |
| `ingress.annotations` | Additional annotations for the Ingress resource. To enable certificate autogeneration, place here your cert-manager annotations. |      `{}`       |
| `ingress.tls`         | Enable TLS configuration for the hosts defined                                                                                   |      `[]`       |
| `ingress.hosts`       | An array with additional hostname(s) to be covered with the ingress record                                                       | see values.yaml |

## Testing

This helm chart can be tested with following steps:

* create local test cluster with `kind` with nfs CSI

```bash
kind create cluster 
helm repo add stable https://charts.helm.sh/stable; helm repo update
helm install --wait nfs-server stable/nfs-server-provisioner
```

* run [ct](https://github.com/helm/chart-testing) in docker

```bash
docker run -it --network host --workdir=/data --volume $(pwd):/data \
   --volume ~/.kube/config:/root/.kube/config:ro \
   quay.io/helmpack/chart-testing:v3.7.1 \
   ct lint-and-install --charts charts/os-image-server --validate-maintainers=false
```
