{{/* Expand the name of the chart. */}}
{{- define "os-image-server.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "os-image-server.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/* Create chart name and version as used by the chart label.*/}}
{{- define "os-image-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/* Common labels */}}
{{- define "os-image-server.labels" -}}
helm.sh/chart: {{ include "os-image-server.chart" . }}
{{ include "os-image-server.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/* Selector labels */}}
{{- define "os-image-server.selectorLabels" -}}
app.kubernetes.io/name: {{ include "os-image-server.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/* Create the name of the service account to use */}}
{{- define "os-image-server.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "os-image-server.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{/* Return the image names */}}
{{- define "getImageName" -}}
{{- printf "%s/%s:%s" ($.registry|default "") $.repository ($.tag|default "") | trimSuffix ":" | trimPrefix "/"  -}}
{{- end -}}

{{/* returns the PV name to use for a given image*/}}
{{/*

We ensure as much as possible, that if the image content changes we'll use a different PV.
This is done by suffixing the PV name with a hash of the OS image data checksum (or as fallback, of the download URI)

*/}}
{{- define "pv-name" -}}
    {{- $envAll := index . 0 }}
    {{- $imageName := index . 1 }}
    {{- $imageParams := index . 2 }}

    {{- $pv_name_suffix := $imageParams.uri | sha256sum | trunc 8 -}}
    {{- if (hasKey $imageParams "sha256") -}}
        {{- $pv_name_suffix = $imageParams.sha256 | trunc 8 -}}
    {{- end -}}
{{- include "os-image-server.fullname" $envAll }}-{{ $imageName }}-{{ $pv_name_suffix -}}
{{- end -}}
