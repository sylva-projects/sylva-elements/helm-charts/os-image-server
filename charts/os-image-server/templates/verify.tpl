# This template do not produce any object
# It only computes some variables for checking purpose

{{/* Check filenames unicity */}}
{{- $filenames := dict -}}
{{- range $images := tuple .Values.os_images .Values.os_image_selectors | include "select-os-images" | fromJson -}}
{{- if hasKey $filenames $images.filename -}}
    {{- printf "[ERROR] Duplicate filename %s - Not supported" $images.filename | fail -}}
{{- else -}}
    {{- $_ := set $filenames $images.filename "" -}}
{{- end -}}
{{- end -}}
