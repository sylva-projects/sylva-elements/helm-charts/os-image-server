#!/bin/sh

set -eo pipefail

export URI_DETECTION_FILE="image-url.txt"

function oci_download {
  echo "downloading OCI artifact $URI"
  mkdir -p ./oci_tmp
  flux pull artifact --timeout="$OCI_TIMEOUT" $URI -o ./oci_tmp
  downloaded_files_count="$(ls -1 ./oci_tmp/ | grep -v '.sha256sum' | wc -l)"
  if [ "$downloaded_files_count" -gt 1 ];then
    >&2 echo "[ERROR] OCI artifact contains several files:"
    >&2 echo "[ERROR] $(ls ./oci_tmp/)"
    exit 1
  fi
  file=$(ls -1 ./oci_tmp/ | grep -v '.sha256sum' | head -1)

  if $(echo "$file" | grep -q '[.]gz$'); then
    mv -v "./oci_tmp/$file" "$FILENAME.gz"
  else
    mv -v "./oci_tmp/$file" "$FILENAME"
  fi
  rm -r ./oci_tmp
}

function web_download {
  echo "downloading $URI with curl"
  curl -L $URI -o downloaded
  mv -v downloaded "$FILENAME"

  CALCULATED_CHECKSUM=$(sha256sum $FILENAME | awk '{ print $1 }')
  verify_checksum
  echo "$CALCULATED_CHECKSUM" > ${FILENAME}.sha256sum
}

function check_uri_as_changed {
  # maybe URI as changed but filename remain the same
  # this case must be detected and the file to be deleted for new version to be downloaded
  if [ -f "$URI_DETECTION_FILE" ] && [ "$(cat $URI_DETECTION_FILE)" != "$URI" ]; then
    echo "Image URL changed (new: $URI, old: $(cat $URI_DETECTION_FILE)). File $FILENAME must be downloaded."
    rm -fv "$FILENAME"
  fi
  echo "$URI" > $URI_DETECTION_FILE
}

function delete_old_files {
  # Remove all files except FILENAME and URI_DETECTION_FILE
  echo "Cleaning old files"
  find . -type f  ! -name "$FILENAME" ! -name "$FILENAME.gz" ! -name "$URI_DETECTION_FILE" | xargs -tr rm -vr
  rm -rf ./oci_tmp 
}

function verify_checksum {
  if [ -n "$CHECKSUM" ]; then
    echo "Verifying checksum for file $FILENAME"
    if [[ "$CALCULATED_CHECKSUM" != "$CHECKSUM" ]]; then
      print_checksum_error_message
      exit 1;
    fi
  else
   echo "No checksum provided for $FILENAME. Assuming it is OK"
  fi
}

function print_checksum_error_message {
  >&2 echo "[ERROR] checksum mismatch"
  >&2 echo "[ERROR]   calculated checksum = $CALCULATED_CHECKSUM"
  >&2 echo "[ERROR]   expected checksum - $CHECKSUM"
  >&2 echo ""
  >&2 echo "[WARN] file is already downloaded but won't retry"
  >&2 echo "[WARN] to trigger another attempt:"
  >&2 echo "[WARN]      kubectl -n {{ $.Release.Namespace }} scale statefulset ${HOSTNAME%-*} --replicas 0"
  >&2 echo "[WARN]      kubectl -n {{ $.Release.Namespace }} delete pvc ${HOSTNAME}"
  >&2 echo "[WARN]      kubectl -n {{ $.Release.Namespace }} scale statefulset ${HOSTNAME%-*} --current-replicas=0 --replicas 1"
}

cd /var/www/os-images/

# Image download is delegated to the first pod of the statefulset
if [ "$HOSTNAME" == "${HOSTNAME%-*}-0" ]; then

  check_uri_as_changed
  delete_old_files

  if [ -f "$FILENAME" ] || [ -f "$FILENAME.gz" ]; then
    echo "OS image $FILENAME already downloaded"
  else
    [[ "$URI" =~ '^http*' ]] && web_download
    [[ "$URI" =~ '^oci*' ]] && oci_download
    echo "$FILENAME succesfully downloaded"
  fi

else

  while ! ([ -f "$FILENAME" ] || [ -f "$FILENAME.gz" ]); do
    echo "Waiting $FILENAME to be downloaded by ${HOSTNAME%-*}-0"
    sleep 10
  done

fi
